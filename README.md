Проект в Gitlab  состоит из репозитория приложения yelb https://gitlab.com/ip5751367/graduation_work, а также отдельного репозитория под IaC - для хранения там конфигурации инфраструктуры в Terraform https://gitlab.com/ip5751367/iac. Приложение запускается по адресу https://ua6mq.ml/ (Домен на freenom.com). Используются бесплатные сертификаты letsencrypt. Предполагаю, что мой адрес, в процессе отладки, в связи с большим количеством запросов на выпуск сертификатов забанен, вынужден был использовать staging server: (https://acme-staging-v02.api.letsencrypt.org/directory). Сертификат от него untrusted, но проходит проверку по оценке "A" на https://www.ssllabs.com.

<br/><br/>
![такая оценка сайта](images/3.png)

В первом репозитории располагается приложение. Деплой приложения в кластер происходит при помощи helm chart. При этом запускается приложение с redis, pvc, cloud sql прокси и бэкенд/фронтенд. взаимодействие с Cloud SQL происходит посредством sql прокси.

Все компоненты инфраструктуры описаны в Terraform. Вручную заранее создан bucket для хранения Terraform state для обеспечения работы Terraform:

  - backend - модуль терраформа для настройки сети;
  - gke - модуль терраформа для настройки кластера;
  - vds - модуль терраформа для настройки виртуальной машины под gitlab раннер;
  - cloudsql - модуль терраформа для настройки cloud sql.

1. Создание проекта в Google Cloud Platform

До запуска gitlab необходим рабочий раннер в облаке, следовательно, первый запуск необходимо провести с локального компьютера. Необходимо установить на рабочем локальном компьютере gcloud утилиты, терраформ и kubectl.

В консоли облака

    - создан новый проект — s029901, его уникальный id — s029901.

В iam-admin/serviceaccounts

    - создан сервис аккаунт с названием terrafrom, роль owner.

Для него создаем ключ JSON:

    - gcloud iam service-accounts keys create terraform.json --iam-account=terraform@s029901.iam.gserviceaccount.com

Включены api, необходимые для работы в рамках ТЗ:

    - gcloud config set project s029901
    - gcloud auth activate-service-account --key-file=terraform.json
    - gcloud services enable compute.googleapis.com  container.googleapis.com  sql-component.googleapis.com sqladmin.googleapis.com  servicenetworking.googleapis.com cloudresourcemanager.googleapis.com dns.googleapis.com

Определяем зону/регион:

    - gcloud config set compute/zone europe-west2-a
    - gcloud config set compute/region europe-west2

2. В Cloud Storage создаем хранилище s3 с именем s029901-terraform-state:

  	- gsutil mb -p s029901 -l europe-west2 -c regional -b on gs://s029901-terraform-state

3. Запускаем терраформ вначале с локального компа. Из настроек раннера извлекаем токен, отключаем shared runners, придумаем пароль для postgres.

    - terraform init
    - terraform plan/apply -var="gitlab_runner_registration_token=TOKEN" -var="project_id=s029901" -var="sql_pass=SQL_PASS"

4. Для настройки репозиториев прописываем в CI переменные:

    - SERVICEACCOUNT - cat terraform.json  | base64 -w0
    - PROJECTID - s029901
    - RUNNER_TOKEN - <TOKEN>
    - SQL_PASS (пароль юзера postgres) - <SQL_PASS>

Конфигурация кластера:

    - gcloud container clusters get-credentials gke-prod-cluster
    - API кластера (kubectl cluster-info) добавляем K8S_API_URL (vars graduation_work)

5. Устанавливаем ingress контроллер в кластер:

    - helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
    - helm repo update
    - kubectl create ns nginx
    - helm install nginx ingress-nginx/ingress-nginx --namespace nginx --set rbac.create=true --set controller.publishService.enabled=true

6. Устанавливаем cert-manager :

    - kubectl apply --validate=false -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.12/deploy/manifests/00-crds.yaml
    - kubectl create namespace cert-manager
    - helm repo add jetstack https://charts.jetstack.io
    - helm repo update
    - helm install cert-manager  --namespace cert-manager  --version v0.12.0  --set ingressShim.defaultIssuerName=letsencrypt  --set ingressShim.defaultIssuerKind=ClusterIssuer  jetstack/cert-manager

7. После того, как кластер создан находим ip LoadBalancer - и заносим ip в А запись в днс своего домена:

    - kubectl get service -A

8. Создаем и настраиваем аккаунт для входа:

    - kubectl create namespace prod
    - kubectl create serviceaccount --namespace prod ci
    - cat << EOF | kubectl create --namespace prod -f -
          apiVersion: rbac.authorization.k8s.io/v1
          kind: ClusterRole
          metadata:
            name: prod-ci
          rules:
          - apiGroups: ["", "extensions", "apps", "batch", "events", "certmanager.k8s.io", "cert-manager.io", "monitoring.coreos.com", "networking.k8s.io"]
            resources: ["*"]
            verbs: ["*"]
  EOF

    - kubectl create clusterrolebinding --namespace prod --serviceaccount prod:ci --clusterrole prod-ci prod-ci-binding

9. Добавляем токен в K8S_CI_TOKEN в проекте graduation_work:

    - kubectl get secret --namespace prod $( kubectl get serviceaccount --namespace prod ci -o jsonpath='{.secrets[].name}' ) -o jsonpath='{.data.token}' | base64 -d

10. Создаем image pull secret k8s-pull-token - для того, чтобы кластер Kubernetes мог получать образы из registry gitlab :

    - kubectl create secret docker-registry gitlab-registry --docker-server registry.gitlab.com --docker-email 'example@gmail.com' --docker-username '<первая строчка из окна создания токена в gitlab>' --docker-password '<вторая строчка из окна создания токена в gitlab>' --namespace prod

11. Создаем секрет для бд а также секрет с ключом от сервисного account для sql proxy:

    - kubectl create secret generic db \
      --from-literal=username=postgres \
      --from-literal=password=<YOUR-DATABASE-PASSWORD> \
      --from-literal=connectionname=<YOUR-INSTANCE_CONNECTION_NAME> \
      -n prod

    - kubectl create secret generic cloudsql-instance-credentials --from-file=terraform.json=terraform.json -n prod

12. Cоздаем БД для приложения (psql предварительно уже установлен на VM с раннером при создании VM) с VM раннера:

    - gcloud compute ssh  gitlab-runner
    - sudo -i

    - psql -v ON_ERROR_STOP=1 -h ip_address db  --username postgres -W <<-EOSQL
      CREATE DATABASE yelbdatabase;
      \connect yelbdatabase;
  	CREATE TABLE restaurants (
      	name        char(30),
      	count       integer,
      	PRIMARY KEY (name)
  	);
  	INSERT INTO restaurants (name, count) VALUES ('outback', 0);
  	INSERT INTO restaurants (name, count) VALUES ('bucadibeppo', 0);
  	INSERT INTO restaurants (name, count) VALUES ('chipotle', 0);
  	INSERT INTO restaurants (name, count) VALUES ('ihop', 0);
  EOSQL


13. Logs & monitoring встроенными средствами Google Cloud Platform

<br/><br/>
![метрики контейнеров](images/1.png)

<br/><br/>
![метрики подов](images/2.png)

<br/><br/>
![логи подов](images/4.png)

<br/><br/>
![логи кластера](images/5.png)

<br/><br/>
![логи Cloud SQL Database](images/6.png)

